/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Naty-PC
 */
public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Scanner leer = new Scanner(System.in);
        
        int i=0;
        int opcion=0;
        double n1=0;
        double n2=0;
        
        System.out.print("\nBienvenido al taller 1 Calculadora!\n");
        
        do
        {
            System.out.print("\nSeleccione la opcion que desee utilizar:");
            System.out.print("\n1) Sumar 2 numeros.");
            System.out.print("\n2) Restar 2 numeros.");
            System.out.print("\n3) Multiplicar 2 numeros.");
            System.out.print("\n4) Dividir 2 numeros.");
            System.out.print("\n5) Salir.");
            
            System.out.print("\n>>> .");
            opcion=leer.nextInt();
            
            if(opcion<0 || opcion>5)
            {
                System.out.print("\n\nError!, ingrese un numero valido!.");
            }
            
            if(opcion==1)
            {
                suma sumita = new suma();
                
                System.out.println("Ingrese numero 1: ");
                n1=leer.nextDouble();
                System.out.println("Ingrese numero 2: ");
                n2=leer.nextDouble();
                
                sumita.setNumero1(n1);
                sumita.setNumero1(n2);
                
                sumita.llamar_suma(n1,n2);
                
            }
            
            if(opcion==2)
            {
                resta restitita = new resta();
                
                System.out.println("Ingrese numero 1: ");
                n1=leer.nextDouble();
                System.out.println("Ingrese numero 2: ");
                n2=leer.nextDouble();
                
                restitita.setNumero1(n1);
                restitita.setNumero1(n2);
                
                restitita.llamar_resta(n1,n2); 
            }
            
            if(opcion==3)
            {
                multiplicacion multi = new multiplicacion();
                
                System.out.println("Ingrese numero 1: ");
                n1=leer.nextDouble();
                System.out.println("Ingrese numero 2: ");
                n2=leer.nextDouble();
                
                multi.setNumero1(n1);
                multi.setNumero1(n2);
                
                multi.llamar_multiplicacion(n1,n2); 
            }
            
            if(opcion==4)
            {
                division divi = new division();
                
                System.out.println("Ingrese numero 1: ");
                n1=leer.nextDouble();
                System.out.println("Ingrese numero 2: ");
                n2=leer.nextDouble();
                
                divi.setNumero1(n1);
                divi.setNumero1(n2);
                
                divi.llamar_division(n1,n2); 
            }
            
            
        }while(opcion!=5);
        
        
        System.out.print("\n\nGracias por utilizar este programa!.\n\n");
        
    }
    
}